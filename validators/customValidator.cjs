const customValidator = (directoryName, fileList) => {

    let message = '';

    if (directoryName === undefined && fileList === undefined) {
        message = 'directoryName and fileList required';
    } else if (directoryName === undefined) {
        message = 'directoryName required';
    } else if (fileList === undefined) {
        message = 'fileList required';
    } else if (typeof directoryName !== 'string') {
        message = 'directoryName should be in string only';
    } else if (typeof directoryName === 'string' && directoryName.trim() === '') {
        message = "directoryName can't be empty";
    } else if (typeof directoryName === 'string' && directoryName.includes('/')) {
        message = "directoryName should not contain /";
    } else if (Array.isArray(fileList) === false) {
        message = "fileList should be an array";
    } else if (fileList.length === 0) {
        message = "fileList can't be empty";
    } else {
        const validFilesList = fileList.filter((file) => {
            if (typeof file === 'string' && file.trim() === '') {
                return false;
            } else if (typeof file === 'string' && file.includes('/') === true) {
                return false;
            } else if (typeof file === 'string') {
                return true;
            } else {
                return false;
            }
        }).map((file) => {
            file = file.trim();
            if (file.endsWith('json')) {
                return file;
            } else {
                return `${file}.json`;
            }
        });

        const uniqueFileList = validFilesList.filter((file, index) => {
            return validFilesList.indexOf(file) === index;
        });

        if (validFilesList.length !== fileList.length) {
            message = "All file names in fileList should be string only, and file name should not be empty string or contain /";
        } else if (validFilesList.length !== uniqueFileList.length) {
            message = "Two or more file names can't be same in fileList";
        } else {
            message = "";
        }
    }
    return message;
};

module.exports = customValidator;