const errorHandler = (err, req, res, next) => {
    console.log(err);
    if (err.code === 'ENOENT') {
        let directoryName = err.path.split('/')[err.path.split('/').length - 1];
        res.status(404).json({
            message: `${directoryName} directory not found`
        }).end();
    } else if (err.code === 'EACCES') {
        res.status(403).json({
            message: `Directory can't access, permission denied`
        }).end();
    } else if (err.status !== undefined) {
        res.status(err.status).json({
            message: err.message
        }).end();
    } else {
        res.status(500).json({
            message: 'Something went wrong'
        }).end();
    }
};

module.exports = errorHandler;