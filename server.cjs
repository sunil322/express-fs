const express = require('express');
const createDirectoryFilesRouter = require('./router/createDirectoryFilesRouter.cjs');
const deleteFilesRouter = require('./router/deleteFilesRouter.cjs');
const errorHandler = require('./errorHandler.cjs');

const app = express();
const PORT = process.env.PORT || 8000;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use('/create', createDirectoryFilesRouter);
app.use('/delete', deleteFilesRouter);

app.all('*', (req, res) => {
    res.status(404).json({
        message: "This url doesn't exist"
    }).end();
});

app.use(errorHandler);

app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`);
});