const fs = require('fs');
const path = require('path');

const checkDirectory = (directoryPath) => {
    
    return new Promise((resolve, reject) => {
        fs.access(directoryPath, fs.constants.W_OK ,(error) => {
            if (error) {
                reject(error);
            } else {
                resolve(directoryPath);
            }
        });
    });
};

module.exports = checkDirectory;