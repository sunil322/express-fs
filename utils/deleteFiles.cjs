const fs = require('fs');
const path = require('path');

const deleteFiles = (directoryPath, fileNames) => {

    const promises = fileNames.map((fileName) => {
        if (fileName.endsWith('.json') === false) {
            fileName = `${fileName.trim()}.json`;
        }else{
            fileName = fileName.trim();
        }
        return new Promise((resolve, reject) => {
            const filePath = path.join(directoryPath, fileName);
            fs.unlink(filePath, (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(fileName);
                }
            });
        });
    });

    return Promise.allSettled(promises);
};

module.exports = deleteFiles;