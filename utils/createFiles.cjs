const fs = require('fs');
const path = require('path');

const createFiles = (directoryPath, fileNames) => {

    const filePromises = fileNames.map((fileName, index) => {
        if (fileName.endsWith('.json') === false) {
            fileName = `${fileName.trim()}.json`;
        } else {
            fileName = fileName.trim();
        }
        return new Promise((resolve, reject) => {
            const filePath = path.join(directoryPath, fileName);
            fs.writeFile(filePath, '', (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(fileName);
                }
            });
        });
    });

    return Promise.allSettled(filePromises);
};

module.exports = createFiles;