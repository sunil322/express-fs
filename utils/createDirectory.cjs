const fs = require('fs');
const path = require('path');

const createDirectory = (directoryName) => {
    return new Promise((resolve, reject) => {
        const directoryPath = path.join(__dirname, '../jsonFolder', directoryName);
        fs.mkdir(directoryPath, { recursive: true }, (error) => {
            if (error) {
                reject(error);
            } else {
                resolve(directoryPath);
            }
        });
    });
};

module.exports = createDirectory;