const express = require('express');
const createDirectory = require('../utils/createDirectory.cjs');
const createFiles = require('../utils/createFiles.cjs');
const checkDirectory = require('../utils/checkDirectory.cjs');
const customValidator = require('../validators/customValidator.cjs');
const router = express.Router();

router.post('/', (req, res, next) => {

    if (req.headers['content-type'] !== 'application/json') {
        next({
            status: 415,
            message: "content-type should be json only"
        });
    } else {

        const { directoryName, fileList } = req.body;

        const message = customValidator(directoryName, fileList);

        if (message !== '') {
            next({
                status: 400,
                message
            });
        } else {
            createDirectory(directoryName.trim())
                .then((directoryPath)=>{
                    return checkDirectory(directoryPath);
                })
                .then((directoryPath) => {
                    return createFiles(directoryPath, fileList);
                })
                .then((results) => {

                    const apiResponse = {
                        directoryName: directoryName.trim(),
                        message: []
                    };

                    let createdFilesCount = 0;
                    let errorInCreatingFilesCount = 0;

                    results.map((result) => {
                        if (result.status === 'fulfilled') {
                            apiResponse.message.push({
                                fileName: result.value,
                                status: 'file created'
                            });
                            createdFilesCount += 1;
                        } else {
                            const filePathInArray = result.reason.path.split('/');
                            apiResponse.message.push({
                                fileName: filePathInArray[filePathInArray.length - 1],
                                status: 'Error in creating file'
                            });
                            errorInCreatingFilesCount += 1;
                        }
                        return apiResponse;
                    });

                    let statusCode = '';

                    if (results.length === createdFilesCount) {
                        statusCode = 200;
                    } else if (results.length === errorInCreatingFilesCount) {
                        statusCode = 404;
                    } else {
                        statusCode = 206;
                    }

                    res.status(statusCode).json(apiResponse).end();

                })
                .catch((error) => {
                    next(error);
                });
        }
    }
});

module.exports = router;