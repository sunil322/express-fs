const express = require('express');
const path = require('path');
const checkDirectory = require('../utils/checkDirectory.cjs');
const deleteFiles = require('../utils/deleteFiles.cjs');
const customValidator = require('../validators/customValidator.cjs');
const router = express.Router();

router.delete('/', (req, res, next) => {

    if (req.headers['content-type'] !== 'application/json') {
        next({
            status: 415,
            message: "content-type should be json only"
        });
    } else {

        const { directoryName, fileList } = req.body;

        const message = customValidator(directoryName, fileList);

        const directoryPath = path.join(__dirname,'../jsonFolder',directoryName.trim());

        if (message !== '') {
            next({
                status: 400,
                message
            })
        } else {

            const apiResponse = {
                directoryName: directoryName.trim(),
                message: []
            }

            checkDirectory(directoryPath)
                .then((directoryPath) => {
                    return deleteFiles(directoryPath, fileList);
                })
                .then((results) => {

                    let deletedFilesCount = 0;
                    let notFoundFilesCount = 0;

                    results.map((result) => {
                        if (result.status === 'fulfilled') {
                            apiResponse.message.push({
                                fileName: result.value,
                                status: 'file deleted'
                            });
                            deletedFilesCount += 1;
                        } else {
                            const filePathInArray = result.reason.path.split('/');
                            apiResponse.message.push({
                                fileName: filePathInArray[filePathInArray.length - 1],
                                status: "file not found"
                            });
                            notFoundFilesCount += 1;
                        }
                        return apiResponse;
                    });

                    let statusCode = '';

                    if (results.length === deletedFilesCount) {
                        statusCode = 200;
                    } else if (results.length === notFoundFilesCount) {
                        statusCode = 404;
                    } else {
                        statusCode = 206;
                    }

                    res.status(statusCode).json(apiResponse).end();
                })
                .catch((error) => {
                    console.log(error)
                    next(error);
                });
        }
    }
});

module.exports = router;